package com.jagmeet.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigForConnection {
	
	private static final Logger log = LoggerFactory.getLogger(ConfigForConnection.class);

	
	@Value("${spring.datasource.db1.url}")
	private String url;
	@Value("${spring.datasource.db1.username}")
	private String user;
	@Value("${spring.datasource.db1.password}")
	private String password;
	@Value("${spring.datasource.db1.driver-class-name}")
	private String driverClass;
	
	@Bean
	public Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName(driverClass);
			connection = DriverManager.getConnection(url, user, password);
		} catch (SQLException | ClassNotFoundException e) {
			log.info("Exception Occured While Making the ConnectionWith Database :"+e.getMessage());
		}
		return connection;
	}

}