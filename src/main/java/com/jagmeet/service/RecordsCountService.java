package com.jagmeet.service;

import java.util.Map;

import com.jagmeet.model.RecordsCountMV;

public interface RecordsCountService {

	public Map<String, Object> getRecordsCount(String alertCategory);
	

}
