package com.jagmeet.dao;

import java.util.Map;


public interface RecordsCountDao {
	public int getRecordsCount(String alertCategory);
}
