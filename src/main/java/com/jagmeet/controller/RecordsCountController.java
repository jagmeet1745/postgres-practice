package com.jagmeet.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jagmeet.response.CustomResponse;
import com.jagmeet.service.RecordsCountService;

/***
 * Rest Controller for Alert Category Count
 * 
 * @author Jagmeet
 *
 */
@RestController
@RequestMapping("/api")
public class RecordsCountController {
	@Autowired
	RecordsCountService recordsCountService;

	/***
	 * API to count based on Alert Category
	 * 
	 * @param alertCategory
	 * @return {@code RecordsCountMV}
	 */
	@GetMapping("/count/{alertCategory}")
	public ResponseEntity<CustomResponse> getRecordsCount(@PathVariable("alertCategory") String alertCategory) {
		Map<String, Object> metaInfo = new HashMap<>(2);
		metaInfo.put("Service Name", "Total Records Count");
		Map<String, Object> map = recordsCountService.getRecordsCount(alertCategory);
		return new ResponseEntity<>(new CustomResponse("Records Count Fetched Successfully", map.get("Data"), metaInfo),
				HttpStatus.OK);

	}

}
