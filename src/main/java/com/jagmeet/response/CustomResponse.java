package com.jagmeet.response;

import java.util.Map;
import java.util.Objects;

public class CustomResponse {
	private final boolean success;
	private final String infoMsg;
	private final Map<String, Object> metaInfo;
	private final Object data;

	public CustomResponse(String infoMsg, Map<String, Object> metaInfo) {
		this( infoMsg, null, metaInfo);
	}

	public CustomResponse(String infoMsg, Object data, Map<String, Object> metaInfo) {
		this.success = true;
		this.infoMsg = infoMsg;
		this.data = data;
		this.metaInfo = metaInfo;
	}

	public CustomResponse( String infoMsg) {
		this(infoMsg, null);
	}
	public boolean isSuccess() {
		return success;
	}

	
	public String getInfoMsg() {
		return infoMsg;
	}

	public Map<String, Object> getMetaInfo() {
		return metaInfo;
	}

	public Object getData() {
		return data;
	}

	@Override
	public String toString() {
		return "Success : " + Objects.toString(success) + "Info Code : "
				+ Objects.toString(infoMsg, "InfoMsg is null") + "Meta Info : "
				+ Objects.toString(metaInfo, "InfoCode is null") + "Data : " + Objects.toString(data, "Data is null");
	}
}
