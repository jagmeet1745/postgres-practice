package com.jagmeet.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jagmeet.dao.RecordsCountDao;
import com.jagmeet.model.RecordsCountMV;
import com.jagmeet.service.RecordsCountService;

@Service
public class RecordsCountServiceImpl implements RecordsCountService {

	@Autowired
	RecordsCountDao recordsCountDao;

	@Override
	public Map<String, Object> getRecordsCount(String alertCategory) {
		Map<String, Object> map = new HashMap<>();
		recordsCountDao.getRecordsCount(alertCategory);
		RecordsCountMV recordsCountMV = new RecordsCountMV();
		recordsCountMV.setAlertCategory(alertCategory);
		recordsCountMV.setCount(recordsCountDao.getRecordsCount(alertCategory));
		map.put("Data", recordsCountMV);
		return map;
	}

}
