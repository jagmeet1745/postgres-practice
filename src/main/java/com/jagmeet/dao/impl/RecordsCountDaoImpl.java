package com.jagmeet.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.jagmeet.dao.RecordsCountDao;

@Repository
public class RecordsCountDaoImpl implements RecordsCountDao {
	@Autowired
	private Connection connection;

	@Override
	public int getRecordsCount(String alertCategory) {
		String query = "select * from recordCount('" + alertCategory.toLowerCase() + "');";
		ResultSet extractData = null;
		int count = 0;
		try {
			Statement createStatement = connection.createStatement();
			extractData = createStatement.executeQuery(query);
			while (extractData.next()) {
				count = extractData.getInt("recordcount");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return count;
	}
}